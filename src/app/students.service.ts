import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NgModule } from '@angular/core';
@Injectable({
  providedIn: 'root'
})

@NgModule({
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ]
})
export class StudentsService {

    constructor(private http: HttpClient){};
  

    getStudents(): any {
       return (this.http.get('assets/json/students.json'));

     }

}
