import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NgModule } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

@NgModule({
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ]
})
export class DepartmentsService {

  constructor(private http: HttpClient){};

  getDepartments(): any {
     //Returning values from json file
     return this.http.get('assets/json/departments.json');
   }
}
