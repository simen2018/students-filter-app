import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes,UrlSerializer } from '@angular/router'
import { FormsModule } from '@angular/forms';
import { HttpModule }      from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { View2Component } from './view2/view2.component';
import { View3Component } from './view3/view3.component';
import { HomeComponent } from './home/home.component';
import { StudentsService } from './students.service';

const routes: Routes = [
{path: '',component: HomeComponent},
{path: 'home',component: HomeComponent},
{path: 'view2',component: View2Component},
{path:'view3/:subject/:stud_depart',component :View3Component}
]

@NgModule({
  declarations: [
    AppComponent,
    View2Component,
    View3Component,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(routes,{enableTracing:false}),
    StudentsService,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
