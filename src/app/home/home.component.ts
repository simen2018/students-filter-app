import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../students.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Declaring the Promise, this helps by telling the application to wait for
  //the data to load first before executing the html body component
  filtersLoaded: Promise<boolean>;

  //Holds list of students
  data_Student;

  //Constructing the students data
  constructor(private studentService: StudentsService) {}


  ngOnInit() {
    this.studentService.getStudents().subscribe(res => {

        //Appending the list of students to data_Student
        this.data_Student= res;

         // Setting the Promise as resolved after I have the needed data
        this.filtersLoaded = Promise.resolve(true);
      });


  }

}
