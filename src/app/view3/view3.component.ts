import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import { ActivatedRoute} from '@angular/router';

import { DepartmentsService } from '../departments.service';
import { StudentsService } from '../students.service';


@Component({
  selector: 'app-view3',
  templateUrl: './view3.component.html',
  styleUrls: ['./view3.component.css']
})
export class View3Component implements OnInit {
  // Declaring the Promise, yes! Promise!
  filtersLoaded: Promise<boolean>;

  /*holds the boolean value of true or false. If set to true all of the subjects are shown, else the app only shows 4 or less subjects
  in the subjects-wrapper
  */
  isVisible;

  //Holds the total amounts of students in a given subject
  isEmpty;

  //The total amount of subjects for the selected department
  subject_length;

  //This contains the list of all the subjects of the selected department
  getSubjects;

  // The department of the selected subject
  department;

  //The seleceted subject
  current_Topic;
  //Initializing an empty array to hold students that are in a specific subject
  information = [];


  //Constructing the departments and students data
  constructor(private departmentService: DepartmentsService,
  private studentService: StudentsService,private route: ActivatedRoute) {}

  //Marking the selected subject as active
  ActiveOrNot = function(flag:string,topic:string){

    /* Setting the selected subject as active in the subjects-wrapper div */
    if( flag == topic){

        //Getting the element that needs to be set to active and
        //telling the element to get active
        document.getElementById(flag).className = 'subject-name active';

    }

    //if the subject_length is equal to 0, hide the load more options
    if(this.subject_length == 0){
      //console.log(typeof(this.subject_length));
      //The load more element is obtained
      var LoadMore = document.getElementById('loadMore');

      // the loadmore option is hiddent from view
      LoadMore.style.display = 'none';

    }


  }

  //Reloading the page after the user clicks the subject, enable to show the new
  //data
  reloadPage = function(){

  console.log("Reloading the webpage")
  //reloading the page
   window.location.reload();
}
  //This function shows all of the elements that were hidden from view
  showElements = function(){

    //Showing all of the remaining elements that were previously hidden
    this.isVisible = true;

    //Once the user selects the + more button, the button should be hidden from view
    document.getElementById("loadMore").hidden = true;
  }

  ngOnInit() {

    this.studentService.getStudents().subscribe(res => {
      //Get all of the students in the students.json file
      for (let data of (res).body.studentData.students){

        //If the parameter in the url matches the current student is subject name
        //Push it into the empty array created above
        if (this.route.snapshot.params.subject == data.subject){
          //Pushing the data into the array information = [];
          this.information.push(data);

          // Setting the Promise as resolved after I have the needed data
          this.filtersLoaded = Promise.resolve(true);

        }
      }

      //Returning the total amount of students in the array if any
      this.isEmpty=(this.information).length;

    }); //End of student data

      this.departmentService.getDepartments().subscribe(res => {
        this.department =(this.route.snapshot.params.stud_depart);

        for(let value of (res.departments)){
          if (value.subjects.indexOf(this.route.snapshot.params.subject) >= 0) {
            this.getSubjects = value.subjects

            //Assigning the departments to the department variable
            this.department = value.name;

            //Assigning the current_Topic to the current_Topic variable
            this.current_Topic = this.route.snapshot.params.subject;

            //Making the current topic the first in the array of displayed topics
            if (this.getSubjects.indexOf(this.current_Topic) > 0) {

                /*Getting the current location of the selected stubject in the
                list of subjects, and move it to the first position within
                the array*/
                this.getSubjects.splice(this.getSubjects.indexOf(this.current_Topic), 1);

                // inserting the current topic back into the array
                this.getSubjects.unshift(this.current_Topic);
              }

            //console.log(this.getSubjects);
            this.subject_length = value.subjects.length -4;

            // Setting the Promise as resolved after I have the needed data
            this.filtersLoaded = Promise.resolve(true);

          }
        }

      }); //End of department data
  }

}
