import { Component, OnInit } from '@angular/core';

import { DepartmentsService } from '../departments.service';


@Component({
  selector: 'app-view2',
  templateUrl: './view2.component.html',
  styleUrls: ['./view2.component.css']
})
export class View2Component implements OnInit {
  // Declaring the Promise, yes! Promise!
  filtersLoaded: Promise<boolean>;

  data_Department;

  //Constructing the departments data
  constructor(private departmentService: DepartmentsService) {}

  ngOnInit() {
    this.departmentService.getDepartments().subscribe(res => {

        //Appending the list of departments to data_Department
        this.data_Department= res;

         // Setting the Promise as resolved after I have the needed data
        this.filtersLoaded = Promise.resolve(true);
      });
  }

}
