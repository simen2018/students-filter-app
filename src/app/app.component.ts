import { Component } from '@angular/core';
import {Http} from '@angular/http';
import { Router } from '@angular/router';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'students-app';



  // Declaring the Promise, yes! Promise!
  filtersLoaded: Promise<boolean>;

  data_Student;

  constructor(http:Http) {
      http.get('assets/json/students.json').subscribe(res => {
        this.data_Student = res.json();
        this.filtersLoaded = Promise.resolve(true); // Setting the Promise as resolved after I have the needed data

    });


  };



}
